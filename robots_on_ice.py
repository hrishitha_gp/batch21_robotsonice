dr = [1, 0, 0, -1]
dc = [0, 1, -1, 0]
def solve(r, c, count, m, n, st, st1, st2, st3, pt, flag, counter):
    if count == st1 and (r != pt[0][0] and c != pt[0][1]):
        return
    if count == st2 and (r != pt[1][0] and c != pt[1][1]):
        return
    if count == st3 and (r != pt[2][0] and c != pt[2][1]):
        return
    if r == pt[0][0] and c == pt[0][1] and count != st1:
        return
    if r == pt[1][0] and c == pt[1][1] and count != st2:
        return
    if r == pt[2][0] and c == pt[2][1] and count != st3:
        return
    if count == st:
        if r == 0 and c == 1:
            counter[0] += 1
        return
    for i in range(4):
        x, y = r + dr[i], c + dc[i]
        if x < m and y < n and x >= 0 and y >= 0 and not flag[x][y]:
            flag[x][y] = True
            solve(x, y, count + 1, m, n, st, st1, st2, st3, pt, flag, counter)
            flag[x][y] = False
case_num = 1
while True:
        m, n = map(int, input().split())
        if m + n == 0:
            break
        check_in = input().split()
        pt = [(int(check_in[i]), int(check_in[i+1])) for i in range(0, len(check_in), 2)]
        flag = [[False for _ in range(n)] for _ in range(m)]
        flag[0][0] = True
        counter = [0]
        st = m * n
        st1 = int(st / 4)
        st2 = int(st / 2)
        st3 = int(3 * st /4)
        solve(0, 0, 1, m, n, st, st1, st2, st3, pt, flag, counter)
        print(f"Case {case_num}: {counter[0]}")
        case_num += 1

    
